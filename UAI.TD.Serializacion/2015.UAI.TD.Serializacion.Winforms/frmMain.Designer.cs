﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.Runtime.CompilerServices;
using System.Windows.Forms;

namespace _2015.UAI.TD.Serializacion.Winforms
{
    [Microsoft.VisualBasic.CompilerServices.DesignerGenerated()]
    public partial class frmMain : Form
    {

        // Form overrides dispose to clean up the component list.
        [DebuggerNonUserCode()]
        protected override void Dispose(bool disposing)
        {
            try
            {
                if (disposing && components is object)
                {
                    components.Dispose();
                }
            }
            finally
            {
                base.Dispose(disposing);
            }
        }

        // Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.  
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            _cmdSerializacionBinaria = new Button();
            _cmdSerializacionBinaria.Click += new EventHandler(Button1_Click);
            _cmdSerializacionXML = new Button();
            _cmdSerializacionXML.Click += new EventHandler(Button2_Click);
            _cmdSerializacionJson = new Button();
            _cmdSerializacionJson.Click += new EventHandler(Button3_Click);
            _cmdSerializacionCustom = new Button();
            _cmdSerializacionCustom.Click += new EventHandler(Button4_Click);
            _lstPersonas = new ListBox();
            _optSeleccionado = new RadioButton();
            _optTodo = new RadioButton();
            _cmdDeserializaArrayPersona = new Button();
            _cmdDeserializaArrayPersona.Click += new EventHandler(Button7_Click);
            _cmdDeserializaPersona = new Button();
            _cmdDeserializaPersona.Click += new EventHandler(Button8_Click);
            _lstPersonas2 = new ListBox();
            _cmdValidar = new Button();
            _cmdValidar.Click += new EventHandler(cmdValidar_Click);
            _Button1 = new Button();
            _Button1.Click += new EventHandler(Button1_Click_1);
            SuspendLayout();
            // 
            // cmdSerializacionBinaria
            // 
            _cmdSerializacionBinaria.Location = new Point(12, 47);
            _cmdSerializacionBinaria.Name = "_cmdSerializacionBinaria";
            _cmdSerializacionBinaria.Size = new Size(125, 31);
            _cmdSerializacionBinaria.TabIndex = 0;
            _cmdSerializacionBinaria.Text = "Serializacion Binaria";
            _cmdSerializacionBinaria.UseVisualStyleBackColor = true;
            // 
            // cmdSerializacionXML
            // 
            _cmdSerializacionXML.Location = new Point(12, 84);
            _cmdSerializacionXML.Name = "_cmdSerializacionXML";
            _cmdSerializacionXML.Size = new Size(125, 34);
            _cmdSerializacionXML.TabIndex = 1;
            _cmdSerializacionXML.Text = "Serializacion XML";
            _cmdSerializacionXML.UseVisualStyleBackColor = true;
            // 
            // cmdSerializacionJson
            // 
            _cmdSerializacionJson.Location = new Point(12, 124);
            _cmdSerializacionJson.Name = "_cmdSerializacionJson";
            _cmdSerializacionJson.Size = new Size(125, 34);
            _cmdSerializacionJson.TabIndex = 2;
            _cmdSerializacionJson.Text = "Serializacion JSON";
            _cmdSerializacionJson.UseVisualStyleBackColor = true;
            // 
            // cmdSerializacionCustom
            // 
            _cmdSerializacionCustom.Location = new Point(12, 204);
            _cmdSerializacionCustom.Name = "_cmdSerializacionCustom";
            _cmdSerializacionCustom.Size = new Size(125, 34);
            _cmdSerializacionCustom.TabIndex = 3;
            _cmdSerializacionCustom.Text = "Serializacion Custom";
            _cmdSerializacionCustom.UseVisualStyleBackColor = true;
            // 
            // lstPersonas
            // 
            _lstPersonas.FormattingEnabled = true;
            _lstPersonas.Location = new Point(163, 49);
            _lstPersonas.Name = "_lstPersonas";
            _lstPersonas.Size = new Size(209, 264);
            _lstPersonas.TabIndex = 4;
            // 
            // optSeleccionado
            // 
            _optSeleccionado.AutoSize = true;
            _optSeleccionado.Location = new Point(31, 296);
            _optSeleccionado.Name = "_optSeleccionado";
            _optSeleccionado.Size = new Size(90, 17);
            _optSeleccionado.TabIndex = 5;
            _optSeleccionado.Text = "Seleccionado";
            _optSeleccionado.UseVisualStyleBackColor = true;
            // 
            // optTodo
            // 
            _optTodo.AutoSize = true;
            _optTodo.Checked = true;
            _optTodo.Location = new Point(31, 273);
            _optTodo.Name = "_optTodo";
            _optTodo.Size = new Size(50, 17);
            _optTodo.TabIndex = 6;
            _optTodo.TabStop = true;
            _optTodo.Text = "Todo";
            _optTodo.UseVisualStyleBackColor = true;
            // 
            // cmdDeserializaArrayPersona
            // 
            _cmdDeserializaArrayPersona.Location = new Point(406, 124);
            _cmdDeserializaArrayPersona.Name = "_cmdDeserializaArrayPersona";
            _cmdDeserializaArrayPersona.Size = new Size(158, 34);
            _cmdDeserializaArrayPersona.TabIndex = 8;
            _cmdDeserializaArrayPersona.Text = "Deserializacion Array";
            _cmdDeserializaArrayPersona.UseVisualStyleBackColor = true;
            // 
            // cmdDeserializaPersona
            // 
            _cmdDeserializaPersona.Location = new Point(406, 87);
            _cmdDeserializaPersona.Name = "_cmdDeserializaPersona";
            _cmdDeserializaPersona.Size = new Size(158, 31);
            _cmdDeserializaPersona.TabIndex = 7;
            _cmdDeserializaPersona.Text = "Deserializacion Persona";
            _cmdDeserializaPersona.UseVisualStyleBackColor = true;
            // 
            // lstPersonas2
            // 
            _lstPersonas2.FormattingEnabled = true;
            _lstPersonas2.Location = new Point(634, 49);
            _lstPersonas2.Name = "_lstPersonas2";
            _lstPersonas2.Size = new Size(209, 264);
            _lstPersonas2.TabIndex = 9;
            // 
            // cmdValidar
            // 
            _cmdValidar.Location = new Point(406, 280);
            _cmdValidar.Name = "_cmdValidar";
            _cmdValidar.Size = new Size(158, 33);
            _cmdValidar.TabIndex = 10;
            _cmdValidar.Text = "Validacion XSD";
            _cmdValidar.UseVisualStyleBackColor = true;
            // 
            // Button1
            // 
            _Button1.Location = new Point(12, 164);
            _Button1.Name = "_Button1";
            _Button1.Size = new Size(125, 34);
            _Button1.TabIndex = 11;
            _Button1.Text = "Serializacion SOAP";
            _Button1.UseVisualStyleBackColor = true;
            // 
            // frmMain
            // 
            AutoScaleDimensions = new SizeF(6.0F, 13.0F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(866, 328);
            Controls.Add(_Button1);
            Controls.Add(_cmdValidar);
            Controls.Add(_lstPersonas2);
            Controls.Add(_cmdDeserializaArrayPersona);
            Controls.Add(_cmdDeserializaPersona);
            Controls.Add(_optTodo);
            Controls.Add(_optSeleccionado);
            Controls.Add(_lstPersonas);
            Controls.Add(_cmdSerializacionCustom);
            Controls.Add(_cmdSerializacionJson);
            Controls.Add(_cmdSerializacionXML);
            Controls.Add(_cmdSerializacionBinaria);
            Name = "frmMain";
            Text = "UAI 2015 - Serializacion";
            Load += new EventHandler(frmMain_Load);
            ResumeLayout(false);
            PerformLayout();
        }

        private Button _cmdSerializacionBinaria;

        internal Button cmdSerializacionBinaria
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return _cmdSerializacionBinaria;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (_cmdSerializacionBinaria != null)
                {
                    _cmdSerializacionBinaria.Click -= Button1_Click;
                }

                _cmdSerializacionBinaria = value;
                if (_cmdSerializacionBinaria != null)
                {
                    _cmdSerializacionBinaria.Click += Button1_Click;
                }
            }
        }

        private Button _cmdSerializacionXML;

        internal Button cmdSerializacionXML
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return _cmdSerializacionXML;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (_cmdSerializacionXML != null)
                {
                    _cmdSerializacionXML.Click -= Button2_Click;
                }

                _cmdSerializacionXML = value;
                if (_cmdSerializacionXML != null)
                {
                    _cmdSerializacionXML.Click += Button2_Click;
                }
            }
        }

        private Button _cmdSerializacionJson;

        internal Button cmdSerializacionJson
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return _cmdSerializacionJson;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (_cmdSerializacionJson != null)
                {
                    _cmdSerializacionJson.Click -= Button3_Click;
                }

                _cmdSerializacionJson = value;
                if (_cmdSerializacionJson != null)
                {
                    _cmdSerializacionJson.Click += Button3_Click;
                }
            }
        }

        private Button _cmdSerializacionCustom;

        internal Button cmdSerializacionCustom
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return _cmdSerializacionCustom;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (_cmdSerializacionCustom != null)
                {
                    _cmdSerializacionCustom.Click -= Button4_Click;
                }

                _cmdSerializacionCustom = value;
                if (_cmdSerializacionCustom != null)
                {
                    _cmdSerializacionCustom.Click += Button4_Click;
                }
            }
        }

        private ListBox _lstPersonas;

        internal ListBox lstPersonas
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return _lstPersonas;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (_lstPersonas != null)
                {
                }

                _lstPersonas = value;
                if (_lstPersonas != null)
                {
                }
            }
        }

        private RadioButton _optSeleccionado;

        internal RadioButton optSeleccionado
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return _optSeleccionado;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (_optSeleccionado != null)
                {
                }

                _optSeleccionado = value;
                if (_optSeleccionado != null)
                {
                }
            }
        }

        private RadioButton _optTodo;

        internal RadioButton optTodo
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return _optTodo;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (_optTodo != null)
                {
                }

                _optTodo = value;
                if (_optTodo != null)
                {
                }
            }
        }

        private Button _cmdDeserializaArrayPersona;

        internal Button cmdDeserializaArrayPersona
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return _cmdDeserializaArrayPersona;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (_cmdDeserializaArrayPersona != null)
                {
                    _cmdDeserializaArrayPersona.Click -= Button7_Click;
                }

                _cmdDeserializaArrayPersona = value;
                if (_cmdDeserializaArrayPersona != null)
                {
                    _cmdDeserializaArrayPersona.Click += Button7_Click;
                }
            }
        }

        private Button _cmdDeserializaPersona;

        internal Button cmdDeserializaPersona
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return _cmdDeserializaPersona;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (_cmdDeserializaPersona != null)
                {
                    _cmdDeserializaPersona.Click -= Button8_Click;
                }

                _cmdDeserializaPersona = value;
                if (_cmdDeserializaPersona != null)
                {
                    _cmdDeserializaPersona.Click += Button8_Click;
                }
            }
        }

        private ListBox _lstPersonas2;

        internal ListBox lstPersonas2
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return _lstPersonas2;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (_lstPersonas2 != null)
                {
                }

                _lstPersonas2 = value;
                if (_lstPersonas2 != null)
                {
                }
            }
        }

        private Button _cmdValidar;

        internal Button cmdValidar
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return _cmdValidar;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (_cmdValidar != null)
                {
                    _cmdValidar.Click -= cmdValidar_Click;
                }

                _cmdValidar = value;
                if (_cmdValidar != null)
                {
                    _cmdValidar.Click += cmdValidar_Click;
                }
            }
        }

        private Button _Button1;

        internal Button Button1
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return _Button1;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (_Button1 != null)
                {
                    _Button1.Click -= Button1_Click_1;
                }

                _Button1 = value;
                if (_Button1 != null)
                {
                    _Button1.Click += Button1_Click_1;
                }
            }
        }
    }
}