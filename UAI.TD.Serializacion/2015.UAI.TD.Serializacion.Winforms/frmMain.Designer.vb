﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMain
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.cmdSerializacionBinaria = New System.Windows.Forms.Button()
        Me.cmdSerializacionXML = New System.Windows.Forms.Button()
        Me.cmdSerializacionJson = New System.Windows.Forms.Button()
        Me.cmdSerializacionCustom = New System.Windows.Forms.Button()
        Me.lstPersonas = New System.Windows.Forms.ListBox()
        Me.optSeleccionado = New System.Windows.Forms.RadioButton()
        Me.optTodo = New System.Windows.Forms.RadioButton()
        Me.cmdDeserializaArrayPersona = New System.Windows.Forms.Button()
        Me.cmdDeserializaPersona = New System.Windows.Forms.Button()
        Me.lstPersonas2 = New System.Windows.Forms.ListBox()
        Me.cmdValidar = New System.Windows.Forms.Button()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'cmdSerializacionBinaria
        '
        Me.cmdSerializacionBinaria.Location = New System.Drawing.Point(12, 47)
        Me.cmdSerializacionBinaria.Name = "cmdSerializacionBinaria"
        Me.cmdSerializacionBinaria.Size = New System.Drawing.Size(125, 31)
        Me.cmdSerializacionBinaria.TabIndex = 0
        Me.cmdSerializacionBinaria.Text = "Serializacion Binaria"
        Me.cmdSerializacionBinaria.UseVisualStyleBackColor = True
        '
        'cmdSerializacionXML
        '
        Me.cmdSerializacionXML.Location = New System.Drawing.Point(12, 84)
        Me.cmdSerializacionXML.Name = "cmdSerializacionXML"
        Me.cmdSerializacionXML.Size = New System.Drawing.Size(125, 34)
        Me.cmdSerializacionXML.TabIndex = 1
        Me.cmdSerializacionXML.Text = "Serializacion XML"
        Me.cmdSerializacionXML.UseVisualStyleBackColor = True
        '
        'cmdSerializacionJson
        '
        Me.cmdSerializacionJson.Location = New System.Drawing.Point(12, 124)
        Me.cmdSerializacionJson.Name = "cmdSerializacionJson"
        Me.cmdSerializacionJson.Size = New System.Drawing.Size(125, 34)
        Me.cmdSerializacionJson.TabIndex = 2
        Me.cmdSerializacionJson.Text = "Serializacion JSON"
        Me.cmdSerializacionJson.UseVisualStyleBackColor = True
        '
        'cmdSerializacionCustom
        '
        Me.cmdSerializacionCustom.Location = New System.Drawing.Point(12, 204)
        Me.cmdSerializacionCustom.Name = "cmdSerializacionCustom"
        Me.cmdSerializacionCustom.Size = New System.Drawing.Size(125, 34)
        Me.cmdSerializacionCustom.TabIndex = 3
        Me.cmdSerializacionCustom.Text = "Serializacion Custom"
        Me.cmdSerializacionCustom.UseVisualStyleBackColor = True
        '
        'lstPersonas
        '
        Me.lstPersonas.FormattingEnabled = True
        Me.lstPersonas.Location = New System.Drawing.Point(163, 49)
        Me.lstPersonas.Name = "lstPersonas"
        Me.lstPersonas.Size = New System.Drawing.Size(209, 264)
        Me.lstPersonas.TabIndex = 4
        '
        'optSeleccionado
        '
        Me.optSeleccionado.AutoSize = True
        Me.optSeleccionado.Location = New System.Drawing.Point(31, 296)
        Me.optSeleccionado.Name = "optSeleccionado"
        Me.optSeleccionado.Size = New System.Drawing.Size(90, 17)
        Me.optSeleccionado.TabIndex = 5
        Me.optSeleccionado.Text = "Seleccionado"
        Me.optSeleccionado.UseVisualStyleBackColor = True
        '
        'optTodo
        '
        Me.optTodo.AutoSize = True
        Me.optTodo.Checked = True
        Me.optTodo.Location = New System.Drawing.Point(31, 273)
        Me.optTodo.Name = "optTodo"
        Me.optTodo.Size = New System.Drawing.Size(50, 17)
        Me.optTodo.TabIndex = 6
        Me.optTodo.TabStop = True
        Me.optTodo.Text = "Todo"
        Me.optTodo.UseVisualStyleBackColor = True
        '
        'cmdDeserializaArrayPersona
        '
        Me.cmdDeserializaArrayPersona.Location = New System.Drawing.Point(406, 124)
        Me.cmdDeserializaArrayPersona.Name = "cmdDeserializaArrayPersona"
        Me.cmdDeserializaArrayPersona.Size = New System.Drawing.Size(158, 34)
        Me.cmdDeserializaArrayPersona.TabIndex = 8
        Me.cmdDeserializaArrayPersona.Text = "Deserializacion Array"
        Me.cmdDeserializaArrayPersona.UseVisualStyleBackColor = True
        '
        'cmdDeserializaPersona
        '
        Me.cmdDeserializaPersona.Location = New System.Drawing.Point(406, 87)
        Me.cmdDeserializaPersona.Name = "cmdDeserializaPersona"
        Me.cmdDeserializaPersona.Size = New System.Drawing.Size(158, 31)
        Me.cmdDeserializaPersona.TabIndex = 7
        Me.cmdDeserializaPersona.Text = "Deserializacion Persona"
        Me.cmdDeserializaPersona.UseVisualStyleBackColor = True
        '
        'lstPersonas2
        '
        Me.lstPersonas2.FormattingEnabled = True
        Me.lstPersonas2.Location = New System.Drawing.Point(634, 49)
        Me.lstPersonas2.Name = "lstPersonas2"
        Me.lstPersonas2.Size = New System.Drawing.Size(209, 264)
        Me.lstPersonas2.TabIndex = 9
        '
        'cmdValidar
        '
        Me.cmdValidar.Location = New System.Drawing.Point(406, 280)
        Me.cmdValidar.Name = "cmdValidar"
        Me.cmdValidar.Size = New System.Drawing.Size(158, 33)
        Me.cmdValidar.TabIndex = 10
        Me.cmdValidar.Text = "Validacion XSD"
        Me.cmdValidar.UseVisualStyleBackColor = True
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(12, 164)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(125, 34)
        Me.Button1.TabIndex = 11
        Me.Button1.Text = "Serializacion SOAP"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'frmMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(866, 328)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.cmdValidar)
        Me.Controls.Add(Me.lstPersonas2)
        Me.Controls.Add(Me.cmdDeserializaArrayPersona)
        Me.Controls.Add(Me.cmdDeserializaPersona)
        Me.Controls.Add(Me.optTodo)
        Me.Controls.Add(Me.optSeleccionado)
        Me.Controls.Add(Me.lstPersonas)
        Me.Controls.Add(Me.cmdSerializacionCustom)
        Me.Controls.Add(Me.cmdSerializacionJson)
        Me.Controls.Add(Me.cmdSerializacionXML)
        Me.Controls.Add(Me.cmdSerializacionBinaria)
        Me.Name = "frmMain"
        Me.Text = "UAI 2015 - Serializacion"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents cmdSerializacionBinaria As System.Windows.Forms.Button
    Friend WithEvents cmdSerializacionXML As System.Windows.Forms.Button
    Friend WithEvents cmdSerializacionJson As System.Windows.Forms.Button
    Friend WithEvents cmdSerializacionCustom As System.Windows.Forms.Button
    Friend WithEvents lstPersonas As System.Windows.Forms.ListBox
    Friend WithEvents optSeleccionado As System.Windows.Forms.RadioButton
    Friend WithEvents optTodo As System.Windows.Forms.RadioButton
    Friend WithEvents cmdDeserializaArrayPersona As System.Windows.Forms.Button
    Friend WithEvents cmdDeserializaPersona As System.Windows.Forms.Button
    Friend WithEvents lstPersonas2 As System.Windows.Forms.ListBox
    Friend WithEvents cmdValidar As System.Windows.Forms.Button
    Friend WithEvents Button1 As System.Windows.Forms.Button

End Class
