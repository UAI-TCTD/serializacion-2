﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;
using _2015.UAI.TD.Serializacion.BE;
using _2015.UAI.TD.Serializacion.Core;

namespace _2015.UAI.TD.Serializacion.Winforms
{
    public partial class frmMain
    {
        public frmMain()
        {
            InitializeComponent();
            _cmdSerializacionBinaria.Name = "cmdSerializacionBinaria";
            _cmdSerializacionXML.Name = "cmdSerializacionXML";
            _cmdSerializacionJson.Name = "cmdSerializacionJson";
            _cmdSerializacionCustom.Name = "cmdSerializacionCustom";
            _lstPersonas.Name = "lstPersonas";
            _optSeleccionado.Name = "optSeleccionado";
            _optTodo.Name = "optTodo";
            _cmdDeserializaArrayPersona.Name = "cmdDeserializaArrayPersona";
            _cmdDeserializaPersona.Name = "cmdDeserializaPersona";
            _lstPersonas2.Name = "lstPersonas2";
            _cmdValidar.Name = "cmdValidar";
            _Button1.Name = "Button1";
        }

        public Serializador serializador = new Serializador();
        public List<Persona> _personas = new List<Persona>();
        public List<Persona> _personas2;

        private void cargarArchivo<T>()
        {
            Stream myStream = null;
            var openFileDialog1 = new OpenFileDialog();

            // openFileDialog1.InitialDirectory = "c:\"
            openFileDialog1.Filter = "serialized files (*.dat, *.xml, *.json)|*.dat; *.xml; *.json";
            openFileDialog1.FilterIndex = 1;
            // openFileDialog1.RestoreDirectory = True

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    string ext = Path.GetExtension(openFileDialog1.FileName);
                    myStream = openFileDialog1.OpenFile();
                    if (myStream is object)
                    {
                        // Insert code to read the stream here.

                        var serializator = SerializatorFactory.GetSerializator<T>(ext);
                        var p = serializator.Deserializar(myStream);
                        _personas2 = new List<Persona>();
                        if (p is Persona)
                        {
                            _personas2.Add((Persona)p);
                        }
                        else
                        {
                            _personas2 = (List<Persona>)p;
                        }
                    }

                    lstPersonas2.DataSource = _personas2;
                }
                catch (Exception Ex)
                {
                    MessageBox.Show("Imposible deserializar: " + Ex.Message);
                }
                finally
                {
                    if (myStream is object)
                    {
                        myStream.Close();
                    }
                }
            }
        }

        private void frmMain_Load(object sender, EventArgs e)
        {
            _personas.Add(new Persona("26441334", "Roberto", "Carlos"));
            _personas.Add(new Persona("18112133", "Joaquin", "Sabina"));
            _personas.Add(new Persona("14276579", "Diego", "Maradona"));
            lstPersonas.DataSource = _personas;
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            if (optTodo.Checked)
            {
                serializador.Serializar(new BinarySerializator<List<Persona>>(), _personas);
            }
            else
            {
                serializador.Serializar(new BinarySerializator<Persona>(), lstPersonas.SelectedItem);
            }
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            if (optTodo.Checked)
            {
                serializador.Serializar(new XmlSerializator<List<Persona>>(), _personas);
            }
            else
            {
                serializador.Serializar(new XmlSerializator<Persona>(), lstPersonas.SelectedItem);
            }
        }

        private void Button3_Click(object sender, EventArgs e)
        {
            if (optTodo.Checked)
            {
                serializador.Serializar(new JsonSerializator<List<Persona>>(), _personas);
            }
            else
            {
                serializador.Serializar(new JsonSerializator<Persona>(), lstPersonas.SelectedItem);
            }
        }

        private void Button4_Click(object sender, EventArgs e)
        {
            if (optTodo.Checked)
            {
                serializador.Serializar(new IniSerializator<List<Persona>>(), _personas);
            }
            else
            {
                serializador.Serializar(new IniSerializator<Persona>(), lstPersonas.SelectedItem);
            }
        }

        private void Button8_Click(object sender, EventArgs e)
        {
            //frmMain.cargarArchivo<Persona>();
            cargarArchivo<Persona>();

        }

        private void Button7_Click(object sender, EventArgs e)
        {
            //frmMain.cargarArchivo<List<Persona>>();
            cargarArchivo<List<Persona>>();
        }

        private void cmdValidar_Click(object sender, EventArgs e)
        {
            ValidadorXML.Validar();
        }

        private void Button1_Click_1(object sender, EventArgs e)
        {
            if (optTodo.Checked)
            {
                serializador.Serializar(new SOAPSerializator<List<Persona>>(), _personas);
            }
            else
            {
                serializador.Serializar(new SOAPSerializator<Persona>(), lstPersonas.SelectedItem);
            }
        }
    }
}