﻿using System;
using System.IO;
using _2015.UAI.TD.Serializacion.Core;
using Microsoft.VisualBasic;

namespace _2015.UAI.TD.Serializacion.Winforms
{
    public class Serializador
    {
        private AbstractSerializator _strategy;

        public void SetStrategy(AbstractSerializator strategy)
        {
            _strategy = strategy;
        }

        public object Deserializar(Stream str)
        {
            try
            {
                _strategy.Deserializar(str);
                str.Close();
                Interaction.MsgBox("Deserialización completa!", MsgBoxStyle.Information);
            }
            catch (Exception ex)
            {
                throw;
            }

            return default;
        }

        public void Serializar(AbstractSerializator strategy, object que)
        {
            try
            {
                strategy.Serializar(que);
                Interaction.MsgBox("Serialización completa!", MsgBoxStyle.Information);
            }
            catch (Exception e)
            {
                Interaction.MsgBox("Serialización con errores! " + Constants.vbNewLine + e.Message, MsgBoxStyle.Critical);
            }
        }
    }
}