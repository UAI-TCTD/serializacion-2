﻿using System.IO;
using Newtonsoft.Json;

namespace _2015.UAI.TD.Serializacion.Core
{
    public class JsonSerializator<T> : AbstractSerializator
    {
        public override object Deserializar(Stream str)
        {
            var serializer = new JsonSerializer();
            TextReader tr = new StreamReader(str);
            var o = serializer.Deserialize(tr, typeof(T));
            tr.Close();
            return o;
        }

        public override object Serializar(object que)
        {
            string path;
            fs = FileStreamManager.Instance.CreateFile("json");
            path = fs.Name;
            writer = new StreamWriter(fs);
            var serializer = new JsonSerializer();
            using (writer)
                serializer.Serialize(writer, que);
            writer.Close();
            fs.Close();
            return path;
        }
    }
}