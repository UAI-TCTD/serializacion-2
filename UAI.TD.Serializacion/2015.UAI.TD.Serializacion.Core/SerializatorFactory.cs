﻿
namespace _2015.UAI.TD.Serializacion.Core
{
    public class SerializatorFactory
    {
        public static AbstractSerializator GetSerializator<T>(string ext)
        {
            switch (ext)
            {
                case ".json":
                    {
                        return new JsonSerializator<T>();
                    }

                case ".xml":
                    {
                        return new XmlSerializator<T>();
                    }

                case ".dat":
                    {
                        return new BinarySerializator<T>();
                    }

                case ".soap":
                    {
                        return new SOAPSerializator<T>();
                    }

                default:
                    {
                        return null;
                    }
            }
        }
    }
}