﻿using System;
using System.IO;

namespace _2015.UAI.TD.Serializacion.Core
{
    public class FileStreamManager
    {
        private static FileStreamManager _instance;
        private static readonly object _lock = new object();

        public static FileStreamManager Instance
        {
            get
            {
                lock (_lock)
                {
                    if (_instance == null)
                    {
                        _instance = new FileStreamManager();
                    }
                }

                return _instance;
            }
        }

        public FileStream CreateFile(string ext)
        {
            string file;
            file = string.Format("{0}.{1}", Guid.NewGuid().ToString(), ext);
            if (!Directory.Exists("result"))
                Directory.CreateDirectory("result");
            return new FileStream(@"result\" + file, FileMode.Create);
        }
    }
}