﻿using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace _2015.UAI.TD.Serializacion.Core
{
    public class BinarySerializator<T> : AbstractSerializator
    {
        public override object Deserializar(Stream str)
        {
            var formatter = new BinaryFormatter();
            return formatter.Deserialize(str);
        }

        public override object Serializar(object que)
        {
            fs = FileStreamManager.Instance.CreateFile("dat");
            // writer = New StreamWriter(fs)
            var formatter = new BinaryFormatter();
            formatter.Serialize(fs, que);
            // writer.Close()
            fs.Close();
            return fs.Name;
        }
    }
}