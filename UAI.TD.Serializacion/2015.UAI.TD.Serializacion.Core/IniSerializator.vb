﻿Imports System.IO

Public Class IniSerializator(Of T)
    Inherits AbstractSerializator

    Public Overrides Function Deserializar(str As Stream) As Object
        Dim formatter As New IniFormatter(Of T)

        Return formatter.Deserialize(str)



    End Function

    Public Overrides Function Serializar(que As Object)

        fs = FileStreamManager.Instance.CreateFile("ini")
        Dim formatter As New IniFormatter(Of T)
        formatter.Serialize(fs, que)
        fs.Close()
        Return fs.Name
    End Function
End Class
