﻿using System.IO;

namespace _2015.UAI.TD.Serializacion.Core
{
    public abstract class AbstractSerializator
    {
        public abstract object Serializar(object que);
        public abstract object Deserializar(Stream str);

        protected string _extension;
        protected FileStream fs;
        protected TextWriter writer;
    }
}