﻿Imports System.Xml.Serialization
Imports System.Xml
Imports System.IO
Imports _2015.UAI.TD.Serializacion.BE

Public Class XmlSerialization(Of T)
    Implements IStrategySerialization


    Public Function Deserializar() As Object Implements IStrategySerialization.Deserializar

    End Function

    Public Sub Serializar(que As Object) Implements IStrategySerialization.Serializar
        Dim ser As XmlSerializer = New XmlSerializer(GetType(T))
        Dim fs As New FileStream(String.Format("{0}.xml", Guid.NewGuid.ToString), FileMode.Create)
        Dim writer As TextWriter = New StreamWriter(fs)



        ser.Serialize(writer, que)
        writer.Close()
    End Sub
End Class
