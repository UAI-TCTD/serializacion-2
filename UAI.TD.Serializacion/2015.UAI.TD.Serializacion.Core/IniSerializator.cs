﻿using System.IO;

namespace _2015.UAI.TD.Serializacion.Core
{
    public class IniSerializator<T> : AbstractSerializator
    {
        public override object Deserializar(Stream str)
        {
            var formatter = new IniFormatter<T>();
            return formatter.Deserialize(str);
        }

        public override object Serializar(object que)
        {
            fs = FileStreamManager.Instance.CreateFile("ini");
            var formatter = new IniFormatter<T>();
            formatter.Serialize(fs, que);
            fs.Close();
            return fs.Name;
        }
    }
}