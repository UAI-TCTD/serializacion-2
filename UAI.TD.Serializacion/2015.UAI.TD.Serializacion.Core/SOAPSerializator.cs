﻿using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Soap;

namespace _2015.UAI.TD.Serializacion.Core
{
    public class SOAPSerializator<T> : AbstractSerializator
    {
        public override object Deserializar(System.IO.Stream str)
        {
            object DeserializarRet = default;
            var sf = new SoapFormatter(null, new StreamingContext(StreamingContextStates.File));
            // Deserialize the contents of the stream into an object and close the stream.
            DeserializarRet = sf.Deserialize(str);
            return DeserializarRet;
        }

        public override object Serializar(object que)
        {
            fs = FileStreamManager.Instance.CreateFile("soap");
            var sf = new SoapFormatter(null, new StreamingContext(StreamingContextStates.File));
            // Serialize the array to the file stream, and close the stream.
            sf.Serialize(fs, que);
            fs.Close();
            return fs.Name;
        }
    }
}