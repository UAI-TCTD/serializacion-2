﻿using System;
using System.Xml.Linq;
using System.Xml.Schema;
using Microsoft.VisualBasic;

namespace _2015.UAI.TD.Serializacion.Core
{
    public class ValidadorXML
    {
        private static bool errors = false;
        private static XmlSchemaSet schemas;

        public static void XSDErrors(object o, ValidationEventArgs e)
        {
            Console.WriteLine("{0}", e.Message);
            errors = true;
        }

        public static void Validar()
        {
            var xsdMarkup = XElement.Load(@"XML.Addons\schema.xml", LoadOptions.None);
            schemas = new XmlSchemaSet();
            schemas.Add("", xsdMarkup.CreateReader());
            var doc1 = XDocument.Load(@"XML.Addons\Doc1.xml", LoadOptions.None);
            var doc2 = XDocument.Load(@"XML.Addons\Doc2.xml", LoadOptions.None);
            errors = false;
            doc1.Validate(schemas, XSDErrors);
            Interaction.MsgBox(string.Format("doc1 {0}", Interaction.IIf(errors == true, "es invalido", "es valido")));
            errors = false;
            doc2.Validate(schemas, XSDErrors);
            Interaction.MsgBox(string.Format("doc2 {0}", Interaction.IIf(errors == true, "es invalido", "es valido")));
        }
    }
}