﻿Imports System.Runtime.Serialization
Imports System.IO
Imports System.Reflection


'http://stackoverflow.com/questions/12566946/serialize-a-generic-object-to-ini-text-file-implementing-a-custom-iformatter

Public Class IniFormatter(Of T)
    Implements IFormatter




    Sub New()
        Context = New StreamingContext(StreamingContextStates.All)
    End Sub



    Public Property Binder As SerializationBinder Implements IFormatter.Binder

    Public Property Context As StreamingContext Implements IFormatter.Context

    Public Function Deserialize(serializationStream As IO.Stream) As Object Implements IFormatter.Deserialize

    End Function

    Public Property SurrogateSelector As ISurrogateSelector Implements IFormatter.SurrogateSelector


    Public Sub Serialize(serializationStream As Stream, graph As Object) Implements IFormatter.Serialize

        Dim sw As TextWriter = New StreamWriter(serializationStream)

        Dim IsISerializable As Boolean = TypeOf graph Is ISerializable
        Dim objectType As Type
        objectType = GetType(T)


        If IsISerializable Then
            Dim serializable As ISerializable = graph
            Dim info As SerializationInfo
            info = New SerializationInfo(objectType, New FormatterConverter())
            serializable.GetObjectData(info, Context)

            If Not info Is Nothing Then

                For Each member As SerializationEntry In info

                    If member.ObjectType = GetType(String) Or member.GetType.IsPrimitive Then
                        sw.WriteLine("{0}={1}", member.Name, member.Value.ToString)
                    Else
                        'completar para tipos complejos
                    End If

                Next
            End If
        Else


        End If

        sw.Close()


   




    End Sub
End Class
