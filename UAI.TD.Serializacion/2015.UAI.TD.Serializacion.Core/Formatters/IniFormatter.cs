﻿using System;
using System.IO;
using System.Runtime.Serialization;

namespace _2015.UAI.TD.Serializacion.Core
{


    // http://stackoverflow.com/questions/12566946/serialize-a-generic-object-to-ini-text-file-implementing-a-custom-iformatter

    public class IniFormatter<T> : IFormatter
    {
        public IniFormatter()
        {
            Context = new StreamingContext(StreamingContextStates.All);
        }

        public SerializationBinder Binder { get; set; }
        public StreamingContext Context { get; set; }

        public object Deserialize(Stream serializationStream)
        {
            return default;
        }

        public ISurrogateSelector SurrogateSelector { get; set; }

        public void Serialize(Stream serializationStream, object graph)
        {
            TextWriter sw = new StreamWriter(serializationStream);
            bool IsISerializable = graph is ISerializable;
            Type objectType;
            objectType = typeof(T);
            if (IsISerializable)
            {
                ISerializable serializable = (ISerializable)graph;
                SerializationInfo info;
                info = new SerializationInfo(objectType, new FormatterConverter());
                serializable.GetObjectData(info, Context);
                if (info is object)
                {
                    foreach (SerializationEntry member in info)
                    {
                        if (member.ObjectType == typeof(string) | member.GetType().IsPrimitive)
                        {
                            sw.WriteLine("{0}={1}", member.Name, member.Value.ToString());
                        }
                        else
                        {
                            // completar para tipos complejos
                        }
                    }
                }
            }
            else
            {
            }

            sw.Close();
        }
    }
}