﻿using System.IO;
using System.Xml.Serialization;

namespace _2015.UAI.TD.Serializacion.Core
{
    public class XmlSerializator<T> : AbstractSerializator
    {
        public override object Deserializar(Stream str)
        {
            var serializer = new XmlSerializer(typeof(T));
            TextReader tr = new StreamReader(str);
            var o = serializer.Deserialize(tr);
            tr.Close();
            return o;
        }

        public override object Serializar(object que)
        {
            fs = FileStreamManager.Instance.CreateFile("xml");
            writer = new StreamWriter(fs);
            var ser = new XmlSerializer(typeof(T));
            ser.Serialize(writer, que);
            writer.Close();
            fs.Close();
            return fs.Name;
        }
    }
}