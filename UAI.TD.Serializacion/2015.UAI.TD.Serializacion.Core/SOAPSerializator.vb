﻿Imports System.Runtime.Serialization.Formatters.Soap
Imports System.Runtime.Serialization

Public Class SOAPSerializator(Of T)
    Inherits AbstractSerializator

    Public Overrides Function Deserializar(str As IO.Stream) As Object

        Dim sf As New SoapFormatter(Nothing, New StreamingContext(StreamingContextStates.File))
        ' Deserialize the contents of the stream into an object and close the stream.
        Deserializar = sf.Deserialize(str)

     
    End Function

    Public Overrides Function Serializar(que As Object)
        fs = FileStreamManager.Instance.CreateFile("soap")
        Dim sf As New SoapFormatter(Nothing, New StreamingContext(StreamingContextStates.File))
        ' Serialize the array to the file stream, and close the stream.
        sf.Serialize(fs, que)
        fs.Close()
        Return fs.Name
    End Function
End Class
