﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace _2015.UAI.TD.Serializacion.BE
{
    [Serializable()]
    public class Persona:ISerializable
    {
        [NonSerialized()]
        private DateTime fechaNacimiento;

        public string Dni { get; set; }
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public List<Telefono> Telefono { get; set; }

        public Persona()
        {
        }

        public Persona(string Dni, string Nombre, string Apellido)
        {
            this.Dni = Dni;
            this.Nombre = Nombre;
            this.Apellido = Apellido;
        }

        public Persona(SerializationInfo info, StreamingContext context)
        {
            Nombre = info.GetString("nombre");
            Apellido = info.GetString("apellido");
            Dni = info.GetString("dni");
        }

        public override string ToString()
        {
            return string.Format("{0} {1} dni {2}", Nombre, Apellido, Dni);
        }

        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("nombre", "pepe", typeof(string));
            info.AddValue("apellido", Apellido, typeof(string));
            info.AddValue("dni", Dni, typeof(string));
            info.AddValue("tipo", "otro tipo de datos custom", typeof(string)); // esto se usa cuando se serializa con IFormatter (ver XML que es serializer y SOAP que es formatter)
        }
    }
}