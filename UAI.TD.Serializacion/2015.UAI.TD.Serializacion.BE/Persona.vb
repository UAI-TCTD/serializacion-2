﻿Imports System.Runtime.Serialization

<Serializable()>
Public Class Persona
    Implements ISerializable


    Public Property Dni As String
    Public Property Nombre As String
    Public Property Apellido As String
    Public Property Telefono As List(Of Telefono)


    Public Sub New()

    End Sub
    Public Sub New(Dni As String, Nombre As String, Apellido As String)
        Me.Dni = Dni
        Me.Nombre = Nombre
        Me.Apellido = Apellido
    End Sub

    Public Sub New(info As SerializationInfo, context As StreamingContext)

        Nombre = info.GetString("nombre")
        Apellido = info.GetString("apellido")
        Dni = info.GetString("dni")
    End Sub

    Public Overrides Function ToString() As String
        Return String.Format("{0} {1} dni {2}", Nombre, Apellido, Dni)
    End Function



    Public Sub GetObjectData(info As SerializationInfo, context As StreamingContext) Implements ISerializable.GetObjectData
        info.AddValue("nombre", "pepe", GetType(String))
        info.AddValue("apellido", Apellido, GetType(String))
        info.AddValue("dni", Dni, GetType(String))
        info.AddValue("tipo", "otro tipo de datos custom", GetType(String)) 'esto se usa cuando se serializa con IFormatter (ver XML que es serializer y SOAP que es formatter)
    End Sub
End Class
