﻿using System;

namespace _2015.UAI.TD.Serializacion.BE
{
    [Serializable]
    public class Vehiculo
    {
        public string Marca { get; set; }
        public string Modelo { get; set; }
        public string Color { get; set; }
    }
}